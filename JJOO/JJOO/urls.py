"""JJOO URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import patterns,include,url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from sitio import views
from robots_txt.views import RobotsTextView
admin.autodiscover()

urlpatterns = [
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.inicio),
    url(r'^shared/$', views.shared),
    url(r'^inicio/$', views.inicio),
    url(r'^postTweet/$', views.postTweet),
    url(r'^wallPost/$', views.wallPost),
    url(r'^resenias/$', views.resenias_view),
    url(r'^listado_eventos/$', views.listado_eventos_view),
    url(r'^listado_resenias/$', views.listado_resenias_view),
    url(r'^encuestas/$', views.encuestas_view),
    url(r'^listado_encuestas/$', views.listado_encuestas_view),
    url(r'^votar_encuesta/$', views.votar_encuesta_view),
    url(r'^eventos/$', views.eventos_view),
    url(r'^denunciar_contenido/$', views.denunciar_contenido_view),
    url(r'^editar_datos_usuario/', views.editar_datos_usuario_view),
    url(r'^listado_resenias_usuario/', views.listado_resenias_usuario_view),
    url(r'^listado_transmisiones/', views.listado_transmisiones_view),
	url(r'^listado_transmisiones_evento/', views.listado_transmisiones_evento_view),
	url(r'^listado_imagenes_evento/', views.listado_imagenes_evento_view),
	url(r'^listado_resenias_evento/', views.listado_resenias_evento_view),
	url(r'^listado_encuestas_evento/', views.listado_encuestas_evento_view),
    url(r'^transmisiones/', views.transmisiones_view),
    url(r'^listado_encuestas_usuario/', views.listado_encuestas_usuario_view),
    url(r'^editar_resenia/', views.editar_resenia_view),
    url(r'^ver_resenia/', views.ver_resenia_view),
    url(r'^ver_encuesta/', views.ver_encuesta_view),
    url(r'^ver_pregunta/', views.ver_pregunta_view),
    url(r'^ver_transmision/', views.ver_transmision_view),
    url(r'^editar_encuesta/', views.editar_encuesta_view),
    url(r'^editar_transmision/', views.editar_transmision_view),
    url(r'^subir_imagen/', views.subir_imagen_view),
    url(r'^listado_imagenes/', views.listado_imagenes_view),
    url(r'^eliminar_contenido/', views.eliminar_contenido_view),
    url(r'^pregunta_foro/', views.pregunta_foro_view),
    url(r'^respuesta_foro/', views.respuesta_foro_view),
    url(r'^listado_preguntas/', views.listado_preguntas_view),
    url(r'^votar_resenia/', views.votar_resenia_view),
    url(r'^search/', include('haystack.urls')),
    url(r'^robots.txt$', RobotsTextView.as_view()),
    url(r'^listado_transmisiones_usuario/', views.listado_transmisiones_usuario_view),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

