from django import forms
from sitio.models import User, DatosUsuario, Resenias, Encuestas, Comentarios, Imagenes, Transmisiones, Pregunta_Foro, Respuesta_Foro, Comentarios_Resenias


class form_login(forms.ModelForm):
    class Meta:
        model = User
        fields = ['password','username']

class form_register(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username','email','password']

class form_resenias(forms.ModelForm):
    class Meta:
        model = Resenias
        fields = ['titulo', 'contenido']

class form_resenias_2(forms.ModelForm):
    class Meta:
        model = Resenias
        fields = ['titulo', 'contenido', 'id_evento']
		
class form_encuestas(forms.ModelForm):
    class Meta:
        model = Encuestas
        exclude = ['id_evento','id_usuario_creador', 'estado_eliminado']

class form_encuestas_2(forms.ModelForm):
    class Meta:
        model = Encuestas
        exclude = ['id_usuario_creador', 'estado_eliminado']
		
class form_comentarios(forms.ModelForm):
    class Meta:
        model = Comentarios
        fields = ['descripcion']

class form_comentarios_resenias(forms.ModelForm):
    class Meta:
        model = Comentarios_Resenias
        fields = ['descripcion']
		
class form_datos_usuario(forms.ModelForm):
    class Meta:
        model = DatosUsuario
        fields = ['localidad', 'fecha_nacimiento', 'sexo']

class form_imagenes(forms.ModelForm):
    class Meta:
        model= Imagenes
        fields=['id_evento', 'imagen', 'descripcion']

class form_transmisiones(forms.ModelForm):
    class Meta:
        model = Transmisiones
        fields=['canal_tv', 'link', 'fecha_transmision', 'hora_transmision']

class form_transmisiones_2(forms.ModelForm):
    class Meta:
        model = Transmisiones
        fields=['canal_tv', 'link', 'fecha_transmision', 'hora_transmision', 'id_evento']

class form_preguntas(forms.ModelForm):
    class Meta:
        model = Pregunta_Foro
        fields = ['pregunta']

class form_respuestas(forms.ModelForm):
    class Meta:
        model = Respuesta_Foro
        fields = ['respuesta']