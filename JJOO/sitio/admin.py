from django.contrib import admin
from sitio.models import DatosUsuario, Comentarios_Resenias, Pregunta_Foro, Respuesta_Foro, Deporte, Resenias, Paises, Estadios, Localidades, Eventos, Usuarios_Historial, Encuestas, Valores_Encuestas, Resultados_Encuestas, Eventos_Paises, Comentarios, Denuncias_Comentarios, Denuncias_Comentarios_Resenias ,Denuncias_Imagenes, Denuncias_Transmisiones, Denuncias_Resenias, Denuncias_Encuestas, Denuncias_Preguntas, Denuncias_Respuestas, Motivos, Imagenes, Transmisiones, Resenias_Valoraciones

# Register your models here.

class AdminUsuario(admin.ModelAdmin):
    list_display = ('id_usuario','fecha_nacimiento')
    list_filter = ('id_usuario','fecha_nacimiento')

class AdminTransmisiones(admin.ModelAdmin):
    list_display = ('id_evento','canal_tv')
    list_filter = ('id_evento','canal_tv')

class AdminDeportes(admin.ModelAdmin):
    list_display= ('nombre', 'se_juega_en_equipo')
    list_filter= ('nombre', 'se_juega_en_equipo')

class AdminResenias(admin.ModelAdmin):
    list_display = ('titulo', 'contenido', 'id', 'id_usuario_autor')
    list_filter = ('titulo', 'contenido', 'id', 'id_usuario_autor')

class AdminPaises(admin.ModelAdmin):
    list_display= ('nombre', 'imagen')
    list_filter= ('nombre', 'imagen')

class AdminEstadios(admin.ModelAdmin):
    list_display= ('nombre', 'id_localidad')
    list_filter= ('nombre', 'id_localidad')

class AdminLocalidades(admin.ModelAdmin):
    list_display= ('nombre', 'id')
    list_filter= ('nombre', 'id')
    search_fields = ('nombre', )

class AdminEventos(admin.ModelAdmin):
    list_display= ('fecha_hora', 'id_deporte', 'id_estadio')
    list_filter= ('fecha_hora', 'id_deporte', 'id_estadio')

class AdminEncuestas(admin.ModelAdmin):
    list_display= ('titulo', 'descripcion', 'fecha_cierre', 'id', 'id_usuario_creador')
    list_filter= ('titulo', 'descripcion', 'fecha_cierre', 'id', 'id_usuario_creador')

class AdminValores_Encuestas(admin.ModelAdmin):
    list_display= ('id_encuesta', 'valor')
    list_filter= ('id_encuesta', 'valor')

class AdminResultado_Encuestas(admin.ModelAdmin):
    list_display= ('id_encuesta', 'valor_encuesta','id_usuario')
    list_filter= ('id_encuesta', 'valor_encuesta','id_usuario')

class AdminEventos_Paises(admin.ModelAdmin):
    list_display= ('id_evento', 'id_pais')
    list_filter= ('id_evento', 'id_pais')

class AdminComentarios(admin.ModelAdmin):
    list_display= ('id_evento', 'id_usuario_autor','descripcion')
    list_filter= ('id_evento', 'id_usuario_autor','descripcion')
	
class AdminComentarios_Resenias(admin.ModelAdmin):
    list_display= ('id_resenia', 'id_usuario_autor','descripcion')
    list_filter= ('id_resenia', 'id_usuario_autor','descripcion')
	
class AdminMotivos(admin.ModelAdmin):
    list_display= ('descripcion',)
    list_filter= ('descripcion',)

class AdminImagenes(admin.ModelAdmin):
    list_display = ('id_usuario','descripcion', 'id_evento')
    list_filter = ('id_usuario','descripcion', 'id_evento')
	
class AdminUsuarios_Historial(admin.ModelAdmin):
    list_display = ('id_usuario','id_motivo')
    list_filter = ('id_usuario','id_motivo')

class AdminResenias_Valoraciones(admin.ModelAdmin):
    list_display = ('id_usuario','valor', 'id_resenia')
    list_filter = ('id_usuario','valor', 'id_resenia')

class AdminPregunta_Foro(admin.ModelAdmin):
    list_display = ('id_usuario','pregunta')
    list_filter = ('id_usuario','pregunta')

class AdminRespuesta_Foro(admin.ModelAdmin):
    list_display = ('id_usuario','respuesta')
    list_filter = ('id_usuario','respuesta')

class AdminDenuncias_Comentarios(admin.ModelAdmin):
    list_display= ('id_denunciador', 'id_comentario','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador', 'id_comentario','id_motivo','fecha_hora_denuncia')
	
class AdminDenuncias_Comentarios_Resenias(admin.ModelAdmin):
    list_display= ('id_denunciador', 'id_comentario','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador', 'id_comentario','id_motivo','fecha_hora_denuncia')

class AdminDenuncias_Imagenes(admin.ModelAdmin):
    list_display= ('id_denunciador', 'id_imagen','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador', 'id_imagen','id_motivo','fecha_hora_denuncia')

class AdminDenuncias_Resenias(admin.ModelAdmin):
    list_display= ('id_denunciador','id_Resenias','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador','id_Resenias','id_motivo','fecha_hora_denuncia')
	
	
class AdminDenuncias_Preguntas(admin.ModelAdmin):
    list_display= ('id_denunciador','id_pregunta','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador','id_pregunta','id_motivo','fecha_hora_denuncia')
	
class AdminDenuncias_Respuestas(admin.ModelAdmin):
    list_display= ('id_denunciador','id_respuesta','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador','id_respuesta','id_motivo','fecha_hora_denuncia')

class AdminDenuncias_Encuestas(admin.ModelAdmin):
    list_display= ('id_denunciador', 'id_encuesta','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador', 'id_encuesta','id_motivo','fecha_hora_denuncia')
	
class AdminDenuncias_Transmisiones(admin.ModelAdmin):
    list_display= ('id_denunciador','id_transmision','id_motivo','fecha_hora_denuncia')
    list_filter= ('id_denunciador','id_transmision','id_motivo','fecha_hora_denuncia')	
	
admin.site.register(DatosUsuario, AdminUsuario)
admin.site.register(Pregunta_Foro, AdminPregunta_Foro)
admin.site.register(Respuesta_Foro, AdminRespuesta_Foro)
admin.site.register(Resenias_Valoraciones, AdminResenias_Valoraciones)
admin.site.register(Transmisiones, AdminTransmisiones)
admin.site.register(Deporte, AdminDeportes)
admin.site.register(Resenias, AdminResenias)
admin.site.register(Paises, AdminPaises)
admin.site.register(Estadios, AdminEstadios)
admin.site.register(Localidades, AdminLocalidades)
admin.site.register(Eventos, AdminEventos)
admin.site.register(Encuestas, AdminEncuestas)
admin.site.register(Valores_Encuestas, AdminValores_Encuestas)
admin.site.register(Resultados_Encuestas, AdminResultado_Encuestas)
admin.site.register(Eventos_Paises, AdminEventos_Paises)
admin.site.register(Comentarios, AdminComentarios)
admin.site.register(Comentarios_Resenias, AdminComentarios_Resenias)
admin.site.register(Denuncias_Comentarios, AdminDenuncias_Comentarios)
admin.site.register(Denuncias_Comentarios_Resenias, AdminDenuncias_Comentarios_Resenias)
admin.site.register(Denuncias_Transmisiones, AdminDenuncias_Transmisiones)
admin.site.register(Denuncias_Encuestas, AdminDenuncias_Encuestas)
admin.site.register(Denuncias_Imagenes, AdminDenuncias_Imagenes)
admin.site.register(Denuncias_Resenias, AdminDenuncias_Resenias)
admin.site.register(Denuncias_Preguntas, AdminDenuncias_Preguntas)
admin.site.register(Denuncias_Respuestas, AdminDenuncias_Respuestas)
admin.site.register(Motivos, AdminMotivos)
admin.site.register(Imagenes, AdminImagenes)
admin.site.register(Usuarios_Historial, AdminUsuarios_Historial)
