# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from sitio.models import User, Eventos, Resenias, Comentarios_Resenias, UserProfile, Deporte, Transmisiones, Pregunta_Foro, Respuesta_Foro, Encuestas, Valores_Encuestas, Resultados_Encuestas, Eventos_Paises, Comentarios, Paises, Estadios, Denuncias_Comentarios, Denuncias_Comentarios_Resenias, Denuncias_Encuestas, Denuncias_Imagenes, Denuncias_Transmisiones, Denuncias_Resenias, Denuncias_Preguntas, Denuncias_Respuestas, Motivos, DatosUsuario, Imagenes, Usuarios_Historial, Resenias_Valoraciones
from sitio.forms import form_login, form_register, form_comentarios_resenias, form_resenias, form_encuestas, form_comentarios, form_datos_usuario, form_imagenes, form_transmisiones, form_transmisiones_2, form_preguntas, form_respuestas, form_encuestas_2, form_resenias_2
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from django.core.mail import send_mail
import pdb,hashlib,random,string
from datetime import datetime
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import TemplateView
from django.utils import timezone

from allauth.socialaccount.models import SocialAccount,SocialToken,SocialApp
import tweepy
import facebook
from operator import itemgetter, attrgetter, methodcaller
  
def inicio(request):
	#eventos recientes
    eventos= Eventos.objects.all()
    lista_eventos=[]
    lista_eventos_1=[]
    fecha_hoy= timezone.now()
	
    if (len(eventos)<= 8):
        for i in eventos:
            if (i.fecha_hora >= fecha_hoy):
                i.paises= Eventos_Paises.objects.filter(id_evento=i.id)
                lista_eventos_1.append(i)
    else:
        for i in eventos:
            evento_menor=0
            id_evento_menor=0
            for j in eventos:
                if (i.fecha_hora >= fecha_hoy) and (j.fecha_hora >= fecha_hoy):
                    if i != j:
                        if (i.id not in lista_eventos) and (j.id not in lista_eventos):
                            if (i.fecha_hora <= j.fecha_hora):
                                if (evento_menor==0) or (i.fecha_hora < evento_menor):
                                    evento_menor= i.fecha_hora
                                    id_evento_menor= i.id
                            if (i.fecha_hora > j.fecha_hora):
                                if (evento_menor==0) or (j.fecha_hora < evento_menor):
                                    evento_menor= j.fecha_hora
                                    id_evento_menor= j.id
            if (id_evento_menor !=0) and (evento_menor != 0):
                lista_eventos.append(id_evento_menor)
                if len(lista_eventos)== 8:
                    break
				
        for i in lista_eventos:
            for j in eventos:
                if j.id == i:
                    j.paises= Eventos_Paises.objects.filter(id_evento=j.id)
                    lista_eventos_1.append(j)
    				
    #resenias mas valoradas
    resenias= Resenias.objects.all()
    resenias_dic={}
    lista_resenias=[]
	
    if (len(resenias)<=4):
        for i in resenias:
            if i.estado_eliminado == False:
                lista_resenias.append(i)
    else:
        for i in resenias:
            total_votos=0
            if i.estado_eliminado==False:
                total_votos= Resenias_Valoraciones.objects.filter(id_resenia=i.id).count()
                suma_valoracion=0
                resenias_valoraciones= Resenias_Valoraciones.objects.filter(id_resenia=i.id)
                for j in resenias_valoraciones:
                    suma_valoracion+= j.valor
                if (total_votos == 0):
                    resenias_dic[i.id]= (suma_valoracion)
                else:
                    resenias_dic[i.id]= (suma_valoracion/total_votos)	

        resenias_mas_valoradas={}
        for i in resenias_dic.keys():
            id_mayor=0
            valor_mayor=0
            for j in resenias_dic.keys():
                if i != j:
                    if (i not in resenias_mas_valoradas.keys()) and (j not in resenias_mas_valoradas.keys()):
                        if (resenias_dic[i]> resenias_dic[j]):
                            if (valor_mayor==0) or (resenias_dic[i] > valor_mayor):
                                id_mayor= i
                                valor_mayor= resenias_dic[i]
                        else:
                            if (valor_mayor==0) or (resenias_dic[i] > valor_mayor):
                                id_mayor= j
                                valor_mayor= resenias_dic[j]
		
            if (id_mayor!=0) and (valor_mayor != 0):
                resenias_mas_valoradas[id_mayor]= valor_mayor
                if len(resenias_mas_valoradas)==4:
                    break
		
        lista_resenias=[]
        for i in resenias_mas_valoradas.keys():
            for j in resenias:
                if j.id == i:
                    lista_resenias.append(j)
    
	#Encuestas mas votadas 
    encuestas= Encuestas.objects.all()
    encuestas_dic={}
    lista_encuestas=[]
	
    if (len(encuestas)<=4):
        for i in encuestas:
            if (i.estado_eliminado == False) and (i.fecha_cierre >= fecha_hoy):
                lista_encuestas.append(i)
    else:
        for i in encuestas:
            total_votos=0
            if (i.estado_eliminado==False) and (i.fecha_cierre >= fecha_hoy):
                total_votos= Resultados_Encuestas.objects.filter(id_encuesta=i.id).count()
                encuestas_dic[i.id]= total_votos	

        encuestas_mas_votadas={}
        for i in encuestas_dic.keys():
            id_mayor=0
            valor_mayor=0
            for j in encuestas_dic.keys():
                if i != j:
                    if (i not in encuestas_mas_votadas.keys()) and (j not in encuestas_mas_votadas.keys()):
                        if (encuestas_dic[i]> encuestas_dic[j]):
                            if (valor_mayor==0) or (encuestas_dic[i] > valor_mayor):
                                id_mayor= i
                                valor_mayor= encuestas_dic[i]
                        else:
                            if (valor_mayor==0) or (encuestas_dic[i] > valor_mayor):
                                id_mayor= j
                                valor_mayor= encuestas_dic[j]
		
            if (id_mayor!=0) and (valor_mayor != 0):
                encuestas_mas_votadas[id_mayor]= valor_mayor
                if len(encuestas_mas_votadas)==4:
                    break
		
        lista_encuestas=[]
        for i in encuestas_mas_votadas.keys():
            for j in encuestas:
                if j.id == i:
                    lista_encuestas.append(j)
					
    return render(request, 'inicio.html', {'lista_eventos':lista_eventos_1, 'lista_resenias':lista_resenias, 'lista_encuestas':lista_encuestas})

@login_required(login_url='/accounts/login/')
def resenias_view(request):
    tipo_carga = request.GET.get('tipo_carga')
    if request.method == 'GET':
        if tipo_carga == "con_evento":
            form = form_resenias()
            form.id_evento = request.GET.get('id')
        else:
            form = form_resenias_2()
        form.id_usuario_autor = request.user.id
    else:
        if tipo_carga == "con_evento":
            form = form_resenias(request.POST)
            if form.is_valid():
                evento=Eventos.objects.get(id=request.GET.get('id')) 
                usuario=User.objects.get(id=request.user.id)
                resenia = Resenias(titulo = request.POST.get('titulo'), contenido = request.POST.get('contenido'), id_evento= evento , id_usuario_autor= usuario)  
                resenia.save()
        else:
            form = form_resenias_2(request.POST)
            if form.is_valid():
                usuario=User.objects.get(id=request.user.id)
                resenia = Resenias(titulo = request.POST.get('titulo'), contenido = request.POST.get('contenido'), id_evento= Eventos.objects.get(id=request.POST.get('id_evento')), id_usuario_autor= usuario)  
                resenia.save()
        return HttpResponseRedirect('/listado_resenias/')
    return render(request,'resenias.html',{'form': form})

@login_required(login_url='/accounts/login/')
def encuestas_view(request):
    tipo_carga = request.GET.get('tipo_carga')
    if request.method == 'GET':
        if tipo_carga == "con_evento":
            form = form_encuestas()
            form.id_evento = request.GET.get('id')
        else:
            form= form_encuestas_2()
        form.id_usuario_autor= request.user.id
    else:
        if tipo_carga == "con_evento":
            form = form_encuestas(request.POST)
            if form.is_valid():
                evento=Eventos.objects.get(id=request.GET.get('id'))
                usuario=User.objects.get(id=request.user.id)
                encuesta = Encuestas(titulo = request.POST.get('titulo'), descripcion = request.POST.get('descripcion'), fecha_cierre = request.POST.get('fecha_cierre'), id_evento= evento , id_usuario_creador= usuario)
                encuesta.save()
                encuesta_valor=Encuestas.objects.get(titulo = request.POST.get('titulo'), descripcion = request.POST.get('descripcion'), fecha_cierre = request.POST.get('fecha_cierre'), id_evento= evento , id_usuario_creador= usuario)
        else:
            form= form_encuestas_2(request.POST)
            if form.is_valid():
                usuario=User.objects.get(id=request.user.id)
                encuesta = Encuestas(titulo = request.POST.get('titulo'), descripcion = request.POST.get('descripcion'), fecha_cierre = request.POST.get('fecha_cierre'), id_evento= Eventos.objects.get(id=request.POST.get('id_evento')) , id_usuario_creador= usuario)
                encuesta.save()
                encuesta_valor=Encuestas.objects.get(titulo = request.POST.get('titulo'), descripcion = request.POST.get('descripcion'), fecha_cierre = request.POST.get('fecha_cierre'), id_evento= Eventos.objects.get(id=request.POST.get('id_evento')) , id_usuario_creador= usuario)
        Cantidad_opciones=0
        while True:
            Cantidad_opciones+=1
            opcion= request.POST.get('opcion'+str(Cantidad_opciones))
            if opcion != None:
                try:
                    Valor=Valores_Encuestas.objects.get(id_encuesta=encuesta_valor,valor=opcion)
                except Valores_Encuestas.DoesNotExist:
                    Valor=Valores_Encuestas(id_encuesta=encuesta_valor,valor=opcion)
                    Valor.save()
            else:
                break
        return HttpResponseRedirect('/listado_encuestas/')
    return render(request, 'encuestas.html', {'form': form})

@login_required(login_url='/accounts/login/')
def listado_encuestas_view(request):
    usuario= User.objects.get(id=request.user.id)
    lista_encuestas_1 = Encuestas.objects.all()
    lista_encuestas=[]
    for i in lista_encuestas_1:
        if (i.fecha_cierre>= timezone.now()) and (i.estado_eliminado==False):
            lista_encuestas.append(i)
    page = request.GET.get('page')
    encuestas= Paginacion(lista_encuestas,page)
    lista_votos_usuario = Resultados_Encuestas.objects.filter(id_usuario=usuario).values_list('id', flat=True)
    return render(request, 'encuestas_listado.html', {'encuestas': encuestas, 'usuario':usuario, 'votos_usuario':lista_votos_usuario})

@login_required(login_url='/accounts/login/')
def votar_encuesta_view(request):
    usuario=User.objects.get(id=request.user.id)
    lista_votos_usuario = Resultados_Encuestas.objects.filter(id_usuario=usuario).values_list('id', flat=True)
    if request.method== 'GET':
        encuesta= Encuestas.objects.get(id=request.GET.get('id_encuesta'))
        opciones= Valores_Encuestas.objects.filter(id_encuesta=encuesta)
        total_votos=Resultados_Encuestas.objects.filter(id_encuesta=encuesta).count()
        resultados=[]
        for opcion in opciones:
            cantidad_votos_opcion=Resultados_Encuestas.objects.filter(id_encuesta=encuesta,valor_encuesta=opcion).count()
            if total_votos!=0:
                porcentaje=(cantidad_votos_opcion*100)/total_votos
            else:
                porcentaje=0
            resultados.append((opcion.valor,porcentaje))
        return render(request, 'votar_encuesta.html', {'resultados': resultados,'encuesta':encuesta, 'votos_usuario': lista_votos_usuario})
    else:
        encuesta= Encuestas.objects.get(id=request.POST.get('id_encuesta'))
        valor= Valores_Encuestas.objects.get(id_encuesta=encuesta,valor=request.POST['valor'])
        id_encuesta= request.POST.get('id_encuesta')
        try:
            resultados_encuestas= Resultados_Encuestas.objects.get(id_encuesta=encuesta,id_usuario=usuario)
        except Resultados_Encuestas.DoesNotExist:
            resultados_encuestas= Resultados_Encuestas(id_encuesta=encuesta,valor_encuesta=valor,id_usuario=usuario)
            resultados_encuestas.save()
        next='/votar_encuesta/?id_encuesta='+id_encuesta
        return HttpResponseRedirect(next)
    
def listado_eventos_view(request):
    lista_eventos=[]
    filtro_deporte = request.GET.get('filtro_deporte')
    filtro_estadio = request.GET.get('filtro_estadio')
    if ((filtro_deporte == '') or (filtro_deporte == 'Todos') or (filtro_deporte == None)) and ((filtro_estadio == '') or (filtro_estadio == 'Todos') or (filtro_estadio == None)): 
        lista_eventos = Eventos.objects.filter(fecha_hora__gte = datetime.now())    
    else:
        if ((filtro_deporte != '') and (filtro_deporte != 'Todos') and (filtro_deporte != None)) and ((filtro_estadio != '') and (filtro_estadio != 'Todos') and (filtro_estadio != None)):
            deporte = Deporte.objects.get(nombre = filtro_deporte)
            estadio = Estadios.objects.get(nombre = filtro_estadio)
            lista_eventos = Eventos.objects.filter(id_deporte = deporte.id, id_estadio = estadio.id, fecha_hora__gte = datetime.now())
        else:
            if ((filtro_deporte != '') and (filtro_deporte != 'Todos') and (filtro_deporte != None)):
                deporte = Deporte.objects.get(nombre = filtro_deporte)
                lista_eventos = Eventos.objects.filter(id_deporte = deporte.id, fecha_hora__gte = datetime.now()) 
            else:
                if ((filtro_estadio != '') and (filtro_estadio != 'Todos') and (filtro_estadio != None)):
                    estadio = Estadios.objects.get(nombre = filtro_estadio)
                    lista_eventos = Eventos.objects.filter(id_estadio = estadio.id, fecha_hora__gte = datetime.now()) 
    deportes = Deporte.objects.all()
    estadios = Estadios.objects.all()
    page = request.GET.get('page')
    eventos= Paginacion(lista_eventos,page)
		
    return render(request, 'eventos_listado.html', {'eventos': eventos, 'deportes': deportes, 'estadios':estadios, 'filtro_deporte':filtro_deporte, 'filtro_estadio':filtro_estadio})
	
def Paginacion(lista, page):
    paginator = Paginator(lista, 10)
    try: 
	    elementos = paginator.page(page)
    except PageNotAnInteger:
	    elementos = paginator.page(1)
    except EmptyPage:
	    elementos = paginator.page(paginator.num_pages)
		
    return elementos

@login_required(login_url='/accounts/login/')	
def eventos_view(request):
    usuario=User.objects.get(id=request.user.id)
    if request.method=='GET':
        evento= Eventos.objects.get(id=request.GET.get('id'))
    else:
        form=form_comentarios(request.POST)
        if form.is_valid():
            evento= Eventos.objects.get(id=request.POST.get('id'))
            comentario=Comentarios(id_evento=evento,id_usuario_autor=usuario,descripcion=request.POST.get('descripcion'))
            comentario.save()
    paises= Eventos_Paises.objects.filter(id_evento=evento)
    comentarios= Comentarios.objects.filter(id_evento=evento,estado_eliminado=False)
    return render(request,'eventos.html',{'evento':evento,'paises':paises,'comentarios':comentarios,'usuario':usuario})

@login_required(login_url='/accounts/login/')
def listado_resenias_view(request):
    usuario= User.objects.get(id=request.user.id)
    lista_resenias = Resenias.objects.filter(estado_eliminado=False)
    lista_resenias= calcular_valoracion(lista_resenias)
    lista_resenias_valoraciones = Resenias_Valoraciones.objects.all()
    lista_valoraciones_usuario = Resenias_Valoraciones.objects.filter(id_usuario=usuario).values_list('id', flat=True)
    page = request.GET.get('page')
    resenias= Paginacion(lista_resenias,page)	
    return render(request, 'resenias_listado.html', {'resenias': resenias,'usuario':usuario.id, 'resenias_valoraciones':lista_resenias_valoraciones, 'valoraciones_usuario':lista_valoraciones_usuario})

@login_required(login_url='/accounts/login/')
def denunciar_contenido_view(request):
    if request.method == 'GET':
        tipo_contenido= request.GET.get('tipo_contenido')
        evento= request.GET.get('evento')
        pregunta_f= request.GET.get('id_pregunta')
        motivos= Motivos.objects.all()	
        if tipo_contenido == 'comentario':
            contenido= Comentarios.objects.get(id=request.GET.get('id_comentario'))
        elif tipo_contenido == 'resenia': 
            contenido= Resenias.objects.get(id=request.GET.get('id_resenia'))	
        elif tipo_contenido== 'imagen':
            contenido= Imagenes.objects.get(id=request.GET.get('id_imagen'))  
        elif tipo_contenido == 'respuesta':
            contenido= Respuesta_Foro.objects.get(id=request.GET.get('id_respuesta'))			
        elif tipo_contenido == 'pregunta':
            contenido= Pregunta_Foro.objects.get(id=request.GET.get('id_pregunta'))	
        elif tipo_contenido == 'transmision':
            contenido= Transmisiones.objects.get(id=request.GET.get('id_transmision'))	
        elif tipo_contenido == 'encuesta':
            contenido= Encuestas.objects.get(id=request.GET.get('id_encuesta'))	
        elif tipo_contenido == 'comentario_resenia':
            contenido= Comentarios_Resenias.objects.get(id=request.GET.get('id_comentario'))			
        return render (request, 'denunciar_contenido.html',{'tipo_contenido':tipo_contenido, 'contenido':contenido.id, 'evento':evento, 'pregunta_f':pregunta_f, 'Motivos':motivos})
    else:
        tipo_contenido= request.POST.get('tipo_contenido')
        evento= request.POST.get('evento')
        id_pregunta= request.POST.get('pregunta_f')
        usuario_denunciador=User.objects.get(id=request.user.id)
        motivo=Motivos.objects.get(id=request.POST.get('id_motivo'))
        if tipo_contenido == 'comentario':
            contenido= Comentarios.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario_autor.id)
            next= '/eventos/?id='+evento
            try:
                denuncia_comentario= Denuncias_Comentarios.objects.get(id_comentario=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Comentarios.DoesNotExist:
                denuncia_comentario=Denuncias_Comentarios(id_comentario=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_comentario.save()
        elif tipo_contenido == 'comentario_resenia':
            contenido= Comentarios_Resenias.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario_autor.id)
            resenia=Resenias.objects.get(id=contenido.id_resenia.id)
            if resenia.id_usuario_autor == usuario_denunciador:
                next= '/editar_resenia/?id_resenia='+str(resenia.id)
            else:
                next= '/ver_resenia/?id_resenia='+str(resenia.id)
            try:
                denuncia_comentario= Denuncias_Comentarios_Resenias.objects.get(id_comentario=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Comentarios_Resenias.DoesNotExist:
                denuncia_comentario=Denuncias_Comentarios_Resenias(id_comentario=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_comentario.save()
        elif tipo_contenido == 'resenia': 
            contenido= Resenias.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario_autor.id)
            next= '/listado_resenias/'
            try:
                denuncia_resenia= Denuncias_Resenias.objects.get(id_Resenias=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Resenias.DoesNotExist:
                denuncia_resenia=Denuncias_Resenias(id_Resenias=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_resenia.save()
        elif tipo_contenido == 'imagen':
            contenido= Imagenes.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario.id)
            next= '/listado_imagenes/'
            try:
                denuncia_imagen= Denuncias_Imagenes.objects.get(id_imagen=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Imagenes.DoesNotExist:
                denuncia_imagen=Denuncias_Imagenes(id_imagen=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_imagen.save()
        elif tipo_contenido == 'respuesta':
            contenido= Respuesta_Foro.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario.id)
            next= '/ver_pregunta/?id_pregunta='+str(contenido.id_pregunta_foro.id)+'&tipo_contenido=pregunta'
            try:
                denuncia_respuesta= Denuncias_Respuestas.objects.get(id_respuesta=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Respuestas.DoesNotExist:
                denuncia_respuesta=Denuncias_Respuestas(id_respuesta=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_respuesta.save()
        elif tipo_contenido == 'pregunta':
            contenido= Pregunta_Foro.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario.id)
            next= '/listado_preguntas/'
            try:
                denuncia_pregunta= Denuncias_Preguntas.objects.get(id_pregunta=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Preguntas.DoesNotExist:
                denuncia_pregunta=Denuncias_Preguntas(id_pregunta=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_pregunta.save()
        elif tipo_contenido == 'transmision':
            contenido= Transmisiones.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario.id)
            next= '/listado_transmisiones/'
            try:
                denuncia_transmision= Denuncias_Transmisiones.objects.get(id_transmision=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Transmisiones.DoesNotExist:
                denuncia_transmision=Denuncias_Transmisiones(id_transmision=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_transmision.save()
        elif tipo_contenido == 'encuesta':
            contenido= Encuestas.objects.get(id=request.POST.get('contenido'))
            usuario_denunciado=User.objects.get(id=contenido.id_usuario_creador.id)
            next= '/listado_encuestas/'
            try:
                denuncia_encuesta= Denuncias_Encuestas.objects.get(id_encuesta=contenido,id_denunciador=usuario_denunciador)
                return HttpResponseRedirect(next)
            except Denuncias_Encuestas.DoesNotExist:
                denuncia_encuesta=Denuncias_Encuestas(id_encuesta=contenido,id_denunciador=usuario_denunciador,fecha_hora_denuncia=datetime.now(),id_motivo=motivo)
                denuncia_encuesta.save()
        
        historial=Usuarios_Historial(fecha_hora=datetime.now(),id_usuario=usuario_denunciado,id_motivo=motivo)
        historial.save()
        cantidad_denuncias=Usuarios_Historial.objects.filter(id_usuario=usuario_denunciado).count()
		
        if (cantidad_denuncias % 20) == 0:
            datos_usuario = DatosUsuario.objects.get(id_usuario=usuario_denunciado)
            datos_usuario.estado_denunciado= True
            datos_usuario.save()

        return HttpResponseRedirect(next)

@login_required(login_url='/accounts/login/')
def editar_datos_usuario_view(request):
    id_user= request.user.id
    usuario = get_object_or_404(DatosUsuario,id_usuario=id_user)
    if not request.POST:
        form = form_datos_usuario(instance=usuario)
    else:
        form= form_datos_usuario(data= request.POST, instance= usuario)
        if form.is_valid():
            datos= form.save(commit=False)
            datos.save()
    return render(request, 'editar_datos_usuario.html', {'form':form}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def listado_resenias_usuario_view(request):
    id_user= request.user.id
    resenias = Resenias.objects.filter(id_usuario_autor=id_user, estado_eliminado=False)
    return render(request, 'resenias_listado_usuario.html', {'lista_resenias': resenias})

@login_required(login_url='/accounts/login/')
def editar_resenia_view(request):
    id_user= request.user.id
    if request.method=='GET':
        id_resenia_1=request.GET.get('id_resenia')
        usuario = get_object_or_404(Resenias, id= id_resenia_1)
    else:
        tipo= request.POST.get('tipo')
        id_resenia_1=request.POST.get('id_resenia')
        usuario = get_object_or_404(Resenias, id= id_resenia_1)
        if tipo == "comentario":
            form=form_comentarios(request.POST)
            usuario_autor= User.objects.get(id=id_user)
            if form.is_valid():
                resenia= Resenias.objects.get(id=request.POST.get('id_resenia'))
                comentario=Comentarios_Resenias(id_resenia=resenia,id_usuario_autor=usuario_autor,descripcion=request.POST.get('descripcion'))
                comentario.save()
        else:
            form= form_resenias(data= request.POST, instance= usuario)
            if form.is_valid():
                resenia= form.save(commit=False)
                resenia.save()
    resenia = Resenias.objects.get(id= id_resenia_1)
    resenia_form = form_resenias(instance= usuario)
    valoraciones= Resenias_Valoraciones.objects.filter(id_resenia=id_resenia_1)
    suma=0
    cantidad_votos=0
    for i in valoraciones:
        suma += i.valor
        cantidad_votos +=1
    if cantidad_votos != 0:
        suma= suma / cantidad_votos
    comentarios= Comentarios_Resenias.objects.filter(id_resenia=id_resenia_1,estado_eliminado=False)
    return render(request, 'editar_resenias.html', {'resenia_form':resenia_form, 'valoracion':suma, 'usuario':id_user, 'comentarios':comentarios, 'resenia': resenia}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def listado_encuestas_usuario_view(request):
    id_user= request.user.id
    encuestas = Encuestas.objects.filter(id_usuario_creador=id_user, estado_eliminado=False)
    return render(request, 'encuestas_listado_usuario.html', {'lista_encuestas': encuestas})

@login_required(login_url='/accounts/login/')
def editar_encuesta_view(request):
    id_user= request.user.id
    id_encuesta=request.GET.get('id_encuesta')
    usuario = get_object_or_404(Encuestas,id_usuario_creador=id_user, id=id_encuesta)
    if not request.POST:
        form = form_encuestas(instance=usuario)
    else:
        form= form_encuestas(data= request.POST, instance= usuario)
        if form.is_valid():
            encuesta= form.save(commit=False)
            encuesta.save()
    return render(request, 'editar_encuestas.html', {'form':form}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def subir_imagen_view(request):
    if request.method == 'GET':
        form = form_imagenes(request.POST or None, request.FILES or None)
    else:
        form = form_imagenes(request.POST or None, request.FILES or None)
        if form.is_valid(): 
            usuario=User.objects.get(id=request.user.id)
            imagen = Imagenes(imagen = request.FILES['imagen'], descripcion = request.POST.get('descripcion'), id_evento= Eventos.objects.get(id=request.POST.get('id_evento')) , id_usuario= usuario)  
            imagen.save()
            return HttpResponseRedirect('/listado_imagenes/')
    return render(request,'subir_imagenes.html',{'form': form})
	
@login_required(login_url='/accounts/login/')
def listado_imagenes_view(request):
    usuario= User.objects.get(id=request.user.id)
    evento= request.GET.get('evento_seleccionado')
    lista_imagenes= []
    if ((evento == '') or (evento == 'Todos') or (evento == None)):
        lista_imagenes = Imagenes.objects.filter(estado_eliminado=False)
    else:
        lista_imagenes = Imagenes.objects.filter(id_evento=evento)
    page = request.GET.get('page')
    imagenes= Paginacion(lista_imagenes,page)
    lista_eventos= Eventos.objects.all()
    return render(request, 'listado_imagenes.html', {'imagenes': imagenes, 'usuario': usuario, 'eventos': lista_eventos}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def eliminar_contenido_view(request):
    if request.method == 'GET':
        tipo_contenido= request.GET.get('tipo_contenido')
        evento= request.GET.get('evento')
        pregunta_f= request.GET.get('id_pregunta')
        if tipo_contenido == 'comentario':
            contenido= Comentarios.objects.get(id=request.GET.get('id_comentario'))
        elif tipo_contenido == 'resenia': 
            contenido= Resenias.objects.get(id=request.GET.get('id_resenia'))	
        elif tipo_contenido== 'imagen':
            contenido= Imagenes.objects.get(id=request.GET.get('id_imagen'))  
        elif tipo_contenido == 'respuesta':
            contenido= Respuesta_Foro.objects.get(id=request.GET.get('id_respuesta'))			
        elif tipo_contenido == 'pregunta':
            contenido= Pregunta_Foro.objects.get(id=request.GET.get('id_pregunta'))	
        elif tipo_contenido == 'transmision':
            contenido= Transmisiones.objects.get(id=request.GET.get('id_transmision'))	
        elif tipo_contenido == 'encuesta':
            contenido= Encuestas.objects.get(id=request.GET.get('id_encuesta'))	
        elif tipo_contenido == 'comentario_resenia':
            contenido= Comentarios_Resenias.objects.get(id=request.GET.get('id_comentario'))			
        return render (request, 'eliminar_contenido.html',{'tipo_contenido':tipo_contenido, 'contenido':contenido.id, 'evento':evento, 'pregunta_f':pregunta_f})
    else:
        tipo_contenido= request.POST.get('tipo_contenido')
        evento= request.POST.get('evento')
        id_pregunta= request.POST.get('pregunta_f')
        if tipo_contenido == 'comentario':
            contenido= Comentarios.objects.get(id=request.POST.get('contenido'))
            next= '/eventos/?id='+evento
        elif tipo_contenido == 'comentario_resenia':
            contenido= Comentarios_Resenias.objects.get(id=request.POST.get('contenido'))
            resenia=Resenias.objects.get(id=contenido.id_resenia.id)
            if resenia.id_usuario_autor.id == request.user.id:
                next= '/editar_resenia/?id_resenia='+str(resenia.id)
            else:
                next= '/ver_resenia/?id_resenia='+str(resenia.id)
        elif tipo_contenido == 'resenia': 
            contenido= Resenias.objects.get(id=request.POST.get('contenido'))
            next= '/listado_resenias/'
        elif tipo_contenido == 'imagen':
            contenido= Imagenes.objects.get(id=request.POST.get('contenido'))
            next= '/listado_imagenes/'
        elif tipo_contenido == 'respuesta':
            contenido= Respuesta_Foro.objects.get(id=request.POST.get('contenido'))
            next= '/ver_pregunta/?id_pregunta='+id_pregunta+'&tipo_contenido=pregunta'
        elif tipo_contenido == 'pregunta':
            contenido= Pregunta_Foro.objects.get(id=request.POST.get('contenido'))
            next= '/listado_preguntas/'
        elif tipo_contenido == 'transmision':
            contenido= Transmisiones.objects.get(id=request.POST.get('contenido'))
            next= '/listado_transmisiones/'
        elif tipo_contenido == 'encuesta':
            contenido= Encuestas.objects.get(id=request.POST.get('contenido'))
            next= '/listado_encuestas/'
        contenido.estado_eliminado=True
        contenido.save()
        return HttpResponseRedirect(next)

@login_required(login_url='/accounts/login/')
def transmisiones_view(request):
    tipo_carga = request.GET.get('tipo_carga')
    if request.method == 'GET':
        if tipo_carga == "con_evento":
            form = form_transmisiones()
            form.id_evento= request.GET.get('id')
        else:
            form= form_transmisiones_2()
        form.id_usuario= request.user.id
    else:
        if tipo_carga == "con_evento":
            form = form_transmisiones(request.POST)
            if form.is_valid():
                evento=Eventos.objects.get(id=request.GET.get('id')) 
                usuario=User.objects.get(id=request.user.id)
                transmision = Transmisiones(canal_tv = request.POST.get('canal_tv'), link = request.POST.get('link'), fecha_transmision= request.POST.get('fecha_transmision'), hora_transmision= request.POST.get('hora_transmision'), id_usuario= usuario, id_evento =evento)  
                transmision.save()
        else:
            form = form_transmisiones_2(request.POST)
            if form.is_valid():
                usuario=User.objects.get(id=request.user.id)
                transmision = Transmisiones(canal_tv = request.POST.get('canal_tv'), link = request.POST.get('link'), fecha_transmision= request.POST.get('fecha_transmision'), hora_transmision= request.POST.get('hora_transmision'), id_usuario= usuario, id_evento = Eventos.objects.get(id=request.POST.get('id_evento')))  
                transmision.save()
        return HttpResponseRedirect('/listado_transmisiones/')
    return render(request,'transmisiones.html',{'form': form})

@login_required(login_url='/accounts/login/')	
def listado_transmisiones_view(request):
    usuario= User.objects.get(id=request.user.id)
    page = request.GET.get('page')
    tipo_filtro = request.GET.get('filtro')
    lista_transmisiones= []
    if tipo_filtro == "Futuras":
        lista_transmisiones_1= Transmisiones.objects.all()
        for i in lista_transmisiones_1:
            fecha_hoy= datetime.now()
            if (i.fecha_transmision.day >= fecha_hoy.day) and (i.fecha_transmision.month >= fecha_hoy.month) and (i.fecha_transmision.year >= fecha_hoy.year):
                lista_transmisiones.append(i)
    else:
        lista_transmisiones = Transmisiones.objects.filter(estado_eliminado=False)
    transmisiones= Paginacion(lista_transmisiones,page)
    return render(request, 'listado_transmisiones.html', {'transmisiones': transmisiones, 'usuario':usuario})
	
@login_required(login_url='/accounts/login/')	
def ver_resenia_view(request):
    id_user= request.user.id
    user = User.objects.get(id=id_user)
    if request.method == "GET":
        id_resenia_1=request.GET.get('id_resenia')
        usuario = get_object_or_404(Resenias, id= id_resenia_1)
        resenia_form = form_resenias(instance=usuario)
    else:
        id_resenia_1=request.POST.get('id_resenia')
        usuario = get_object_or_404(Resenias, id= id_resenia_1)
        form=form_comentarios(request.POST)
        if form.is_valid():
            resenia= Resenias.objects.get(id=request.POST.get('id_resenia'))
            comentario=Comentarios_Resenias(id_resenia=resenia,id_usuario_autor=user,descripcion=request.POST.get('descripcion'))
            comentario.save()
            resenia_form = form_resenias(instance= usuario)
    valoraciones= Resenias_Valoraciones.objects.filter(id_resenia=id_resenia_1)
    suma=0
    cantidad_votos=0
    for i in valoraciones:
        suma += i.valor
        cantidad_votos +=1
    if cantidad_votos != 0:
        suma= suma / cantidad_votos
    comentarios= Comentarios_Resenias.objects.filter(id_resenia=id_resenia_1,estado_eliminado=False)
    lista_valoraciones_usuario = Resenias_Valoraciones.objects.filter(id_usuario=user).values_list('id', flat=True)
    return render(request,'ver_resenia.html', {'resenia_form':resenia_form, 'usuario':id_user, 'resenia':usuario, 'valoracion':suma, 'comentarios':comentarios, 'valoraciones_usuario':lista_valoraciones_usuario}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def listado_transmisiones_usuario_view(request):
    id_user= request.user.id
    transmisiones = Transmisiones.objects.filter(id_usuario=id_user, estado_eliminado=False)
    return render(request, 'transmisiones_listado_usuario.html', {'lista_transmisiones': transmisiones})

@login_required(login_url='/accounts/login/')	
def ver_transmision_view(request):
    id_user= request.user.id
    id_transmision=request.GET.get('id_transmision')
    usuario = get_object_or_404(Transmisiones, id=id_transmision)
    if not request.POST:
        transmision = form_transmisiones(instance=usuario)
    else:
        transmision= form_transmisiones(data= request.POST, instance= usuario)
        
    return render(request,'ver_transmision.html', {'transmision': transmision}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def editar_transmision_view(request):
    id_user= request.user.id
    id_transmision=request.GET.get('id_transmision')
    usuario = get_object_or_404(Transmisiones,id_usuario=id_user, id=id_transmision)
    if not request.POST:
        form = form_transmisiones(instance=usuario)
    else:
        form= form_transmisiones(data= request.POST, instance= usuario)
        if form.is_valid():
            transmision= form.save(commit=False)
            transmision.save()
    return render(request, 'editar_transmision.html', {'form':form}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def votar_resenia_view(request):
    usuario= User.objects.get(id=request.user.id)
    if request.method== 'GET':
        resenia=Resenias.objects.get(id=request.GET.get('id_resenia'))
    else:
        usuario=User.objects.get(id=request.user.id)
        resenia=Resenias.objects.get(id=request.POST.get('id_resenia'))
        try:
            valor= Resenias_Valoraciones.objects.get(id_usuario=usuario, id_resenia=resenia)
        except Resenias_Valoraciones.DoesNotExist:
            valor=Resenias_Valoraciones(id_usuario=usuario, id_resenia=resenia, valor= request.POST['valor'])
            valor.save()
    resenias= Resenias.objects.filter(estado_eliminado=False)
    resenias= calcular_valoracion(resenias)
    lista_valoraciones_usuario = Resenias_Valoraciones.objects.filter(id_usuario=usuario).values_list('id', flat=True)
    return render(request,'resenias_listado.html',{'resenias':resenias, 'usuario':usuario, 'valoraciones_usuario': lista_valoraciones_usuario})

@login_required(login_url='/accounts/login/')
def listado_preguntas_view(request):
    usuario= User.objects.get(id=request.user.id)
    lista_preguntas = Pregunta_Foro.objects.filter(estado_eliminado=False)
    page = request.GET.get('page')
    preguntas= Paginacion(lista_preguntas,page)
    return render(request, 'listado_preguntas.html', {'preguntas': preguntas, 'usuario':usuario})

@login_required(login_url='/accounts/login/')
def pregunta_foro_view(request):
    if request.method == 'GET':
        form = form_preguntas()
        form.id_usuario = request.user.id
    else:
        form = form_preguntas(request.POST)
        if form.is_valid(): 
            usuario=User.objects.get(id=request.user.id)
            nueva_pregunta = Pregunta_Foro(pregunta = request.POST.get('pregunta'), id_usuario= usuario)  
            nueva_pregunta.save()
            return HttpResponseRedirect('/listado_preguntas/')
    return render(request,'preguntas_foro.html',{'form':form})

@login_required(login_url='/accounts/login/')
def ver_pregunta_view(request):
    usuario=User.objects.get(id=request.user.id)
    id_pregunta=request.GET.get('id_pregunta')
    pregunta_foro = get_object_or_404(Pregunta_Foro, id=id_pregunta)
    if not request.POST:
        pregunta = form_preguntas(instance=pregunta_foro)
    else:
        pregunta= form_preguntas(data= request.POST, instance= pregunta_foro)
      
    respuestas= Respuesta_Foro.objects.filter(id_pregunta_foro=id_pregunta, estado_eliminado=False)
    return render(request,'ver_pregunta.html', {'pregunta': pregunta, 'respuestas':respuestas, 'usuario':usuario, 'pregunta_foro':pregunta_foro}, context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def respuesta_foro_view(request):
    if request.method == 'GET':
        pregunta= Pregunta_Foro.objects.get(id=request.GET.get('id_pregunta'))
    else:
        form = form_respuestas(request.POST)
        id_pregunta= request.POST.get('id_pregunta')
        next= '/ver_pregunta/?id_pregunta='+id_pregunta+'&tipo_contenido=pregunta'
        if form.is_valid(): 
            usuario=User.objects.get(id=request.user.id)
            pregunta= Pregunta_Foro.objects.get(id=request.POST.get('id_pregunta'))
            nueva_respuesta = Respuesta_Foro(respuesta = request.POST.get('respuesta'), id_usuario= usuario, id_pregunta_foro=pregunta)  
            nueva_respuesta.save()
            next= '/ver_pregunta/?id_pregunta='+id_pregunta+'&tipo_contenido=pregunta'
    return HttpResponseRedirect(next)
	
@login_required(login_url='/accounts/login/')
def ver_encuesta_view(request):
    usuario_1=User.objects.get(id=request.user.id)
    encuesta=request.GET.get('id_encuesta')
    usuario = get_object_or_404(Encuestas, id= encuesta)
    resultados=[]
    if request.method == 'GET':
        encuesta_form = form_encuestas(instance=usuario)
        opciones = Valores_Encuestas.objects.filter(id_encuesta=encuesta)
        total_votos=Resultados_Encuestas.objects.filter(id_encuesta=encuesta).count()
        for opcion in opciones:
            cantidad_votos_opcion=Resultados_Encuestas.objects.filter(id_encuesta=encuesta,valor_encuesta=opcion).count()
            if total_votos!=0:
                porcentaje=(cantidad_votos_opcion*100)/total_votos
            else:
                porcentaje=0
            resultados.append((opcion.valor,porcentaje))
    else:
        encuesta_form= form_encuestas(data= request.POST, instance= usuario)
    return render(request,'ver_encuesta.html', {'encuesta_form':encuesta_form, 'usuario':usuario_1, 'resultados':resultados, 'id_encuesta':encuesta, 'us':usuario}, context_instance=RequestContext(request))
	
def calcular_valoracion(resenias):
    for i in resenias:
        i.valoracion=0
        valoraciones= Resenias_Valoraciones.objects.filter(id_resenia=i.id)
        suma=0
        cantidad_votos=0
        for j in valoraciones:
            suma += j.valor
            cantidad_votos +=1
        if cantidad_votos != 0:
            suma= suma / cantidad_votos
        i.valoracion= suma
    return resenias
	
@login_required(login_url='/accounts/login/')
def listado_transmisiones_evento_view(request):
    usuario= User.objects.get(id=request.user.id)
    evento = Eventos.objects.get(id=request.GET.get('id'))
    lista_transmisiones = Transmisiones.objects.filter(estado_eliminado=False, id_evento=evento)
    page = request.GET.get('page')
    transmisiones= Paginacion(lista_transmisiones,page)
    return render(request, 'listado_transmisiones.html', {'transmisiones': transmisiones, 'usuario':usuario, 'evento':evento})
	
@login_required(login_url='/accounts/login/')
def listado_imagenes_evento_view(request):
    usuario= User.objects.get(id=request.user.id)
    evento = Eventos.objects.get(id=request.GET.get('id'))
    lista_imagenes = Imagenes.objects.filter(id_evento=evento, estado_eliminado=False)
    page = request.GET.get('page')
    imagenes= Paginacion(lista_imagenes,page)
    return render(request, 'listado_imagenes.html', {'imagenes': imagenes, 'usuario': usuario}, context_instance=RequestContext(request))
	
@login_required(login_url='/accounts/login/')
def listado_resenias_evento_view(request):
    usuario= User.objects.get(id=request.user.id)
    evento = Eventos.objects.get(id=request.GET.get('id'))
    lista_resenias = Resenias.objects.filter(id_evento=evento, estado_eliminado=False)
    lista_resenias= calcular_valoracion(lista_resenias)
		
    page = request.GET.get('page')
    resenias= Paginacion(lista_resenias,page)
    return render(request, 'resenias_listado.html', {'resenias': resenias,'usuario':usuario, message:''})
	
@login_required(login_url='/accounts/login/')
def listado_encuestas_evento_view(request):
    usuario= User.objects.get(id=request.user.id)
    evento = Eventos.objects.get(id=request.GET.get('id'))
    lista_encuestas_1 = Encuestas.objects.filter(id_evento=evento)
    lista_encuestas=[]
    for i in lista_encuestas_1:
        if (i.fecha_cierre>= timezone.now()) and (i.estado_eliminado==False):
            lista_encuestas.append(i)
    page = request.GET.get('page')
    encuestas= Paginacion(lista_encuestas,page)
    return render(request, 'encuestas_listado.html', {'encuestas': encuestas, 'usuario':usuario})
    
#############################################################################################################
#Facebook

def autenticarfacebook(request):
    try:
        user = User.objects.get(id=request.user.id)
        token= SocialToken.objects.filter(account__user=user, account__provider='facebook')
        graph = facebook.GraphAPI(token[0])
        return graph
    except:
        return "False"

@login_required(login_url='/accounts/login/')	       
def wallPost(request):		
    graph = autenticarfacebook(request)
    if graph == "False":
        message = "Ocurrio un error al conectar con facebook, asegurese de estar logueado en facebook e intentelo nuevamente."
    else:
        id_resenia_1=request.GET.get('id_resenia')
        resenia = get_object_or_404(Resenias, id= id_resenia_1)
        mensaje = "Mi articulo: "+ resenia.titulo +" ya esta publicado, votalo!"
        #link = "http://jjooseia.herokuapp.com/ver_resenia/?id_resenia="+resenia.id_resenia+"&tipo_contenido=resenia&evento="+resenia.evento
        attachment =  {
            'name': 'JJOOSEIA',
            'link': 'http://jjooseia.herokuapp.com/listado_resenias/',
            'caption': "Mi articulo: "+ resenia.titulo +" ya esta publicado, votalo!",
            'description': 'Conoce mas sobre los JJOO ingresando aqui...',
        }
        try:
            graph.put_wall_post(message=mensaje,attachment= attachment)
            message ="Se ha posteado correctamente!"
        except:
            message = "Ocurrio un error al querer publicar. Por favor asegurese de estar logueado en facebook e intentelo nuevamente."
    usuario= User.objects.get(id=request.user.id)
    lista_resenias = Resenias.objects.filter(estado_eliminado=False)
    lista_resenias= calcular_valoracion(lista_resenias)
		
    page = request.GET.get('page')
    resenias= Paginacion(lista_resenias,page)	
    return render(request, 'resenias_listado.html', {'resenias': resenias,'usuario':usuario, 'message': message})

#############################################################################################################
#Twitter

def autenticar(request):
    try:
        user = User.objects.get(id=request.user.id)
        token= SocialToken.objects.filter(account__user=user, account__provider='twitter')
        social = SocialApp.objects.filter(provider="twitter")
        auth = tweepy.OAuthHandler(social[0].client_id, social[0].secret)
        auth.set_access_token(token[0].token, token[0].token_secret)
        api = tweepy.API(auth)
        return api
    except:
        return "False"
        
#Tweetear compartiendo una reseña
@login_required(login_url='/accounts/login/')	
def postTweet(request):
    api = autenticar(request)
    if api == "False":
        message = "Ocurrio un error al conectar con twitter, asegurese de estar logueado en twitter e intentelo nuevamente."
    else:
        id_resenia_1=request.GET.get('id_resenia')
        resenia = get_object_or_404(Resenias, id= id_resenia_1)
        #link = "http://jjooseia.herokuapp.com/ver_resenia/?id_resenia="+resenia.id_resenia+"&tipo_contenido=resenia&evento="+resenia.evento
        status = "Mi articulo: "+ resenia.titulo +" ya esta publicado, votalo y conoce mas sobre los JJOO ingresando en http://jjooseia.herokuapp.com"
        try:
            api.update_status(status[:138])
            message = "Se ha twitteado correctamente!"
        except:
            message = "Ya ha twitteado esto..."
    usuario= User.objects.get(id=request.user.id)
    lista_resenias = Resenias.objects.filter(estado_eliminado=False)
    lista_resenias= calcular_valoracion(lista_resenias)
		
    page = request.GET.get('page')
    resenias= Paginacion(lista_resenias,page)	
    return render(request, 'resenias_listado.html', {'resenias': resenias,'usuario':usuario, 'message': message})
        
@login_required(login_url='/accounts/login/')
def shared(request):
    user = User.objects.get(id=request.user.id)
    foll = followers(request)
    if request.method == 'POST':
        #Hay q enviar mensaje privado
        message = sendMessage(request)
    else:
        message = ""
    return render(request, 'shared.html', {'usuario': user, 'followers': foll , 'message': message})
            
#Amigos en tw
def followers(request):
    api = autenticar(request)
    lista = []
    if api != "False":
        try:
            for user in tweepy.Cursor(api.followers).items():
                nombre = user.screen_name
                imagen = user.profile_image_url_https
                identificador = user.id
                lista.append([identificador,imagen,nombre,nombre.upper()])
            lista= sorted(lista,key=itemgetter(3))
        except:
            return lista
    return lista

#Envía un mensaje privado a la persona que selecciona, invitandola a que use la página
def sendMessage(request):
    api = autenticar(request)
    user = request.POST.get('follow_id')
    if api != "False":
        try:
            mensaje= "Conoce todo sobre los JJOO en http://jjooseia.herokuapp.com. Yo te lo recomiendo ;) " 
            a = api.send_direct_message(user_id = user, text= mensaje)
            return "Se ha enviado correctamente el mensaje."
        except tweepy.TweepError as e:
            return "Ocurrio un error al conectar con twitter, asegurese de estar logueado en twitter e intentelo nuevamente. Error:"+ e.reason
    return "Ocurrio un error al conectar con twitter, asegurese de estar logueado en twitter e intentelo nuevamente."
	