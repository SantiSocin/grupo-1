from haystack import indexes
from sitio.models import Eventos, Resenias, Encuestas, Transmisiones, Pregunta_Foro, Respuesta_Foro


class EventosIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    deporte = indexes.CharField(model_attr='id_deporte')
    fecha_hora = indexes.CharField(model_attr='fecha_hora')
    estadio = indexes.CharField(model_attr='id_estadio')

    def get_model(self):
        return Eventos

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class ReseniasIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    contenido = indexes.CharField(model_attr='contenido')

    def get_model(self):
        return Resenias

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
		
class EncuestasIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    descripcion = indexes.CharField(model_attr='descripcion')


    def get_model(self):
        return Encuestas

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class TransmisionesIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    canal_tv = indexes.CharField(model_attr='canal_tv')
    link = indexes.CharField(model_attr='link')
    fecha_transmision = indexes.CharField(model_attr='fecha_transmision')


    def get_model(self):
        return Transmisiones

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class Pregunta_ForoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    pregunta= indexes.CharField(model_attr='pregunta')

    def get_model(self):
        return Pregunta_Foro

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
