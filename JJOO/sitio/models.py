# -*- coding: utf-8 -*-
from django.db import models
import random
from django.contrib.auth.models import User
import django_extensions

class DatosUsuario(models.Model):
    fecha_de_baja= models.DateField(null=True, blank=True)
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_nacimiento= models.DateField(null=True, blank=True)
    sexo_choices= (
        ('Masculino', 'Masculino'),
        ('Femenino', 'Femenino'),
    )
    sexo= models.CharField(choices= sexo_choices, max_length=25)
    localidad= models.CharField(max_length=25)
    estado_denunciado= models.BooleanField(default=False)
	
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=40, blank=True)

    def __str__(self):
        return self.user.username	

class Deporte(models.Model):
    nombre= models.CharField(max_length=30, unique=True)
    se_juega_en_equipo = models.BooleanField()
    def __str__(self):
        return self.nombre

class Paises(models.Model):
    nombre= models.CharField(max_length=30 , unique=True)
    imagen= models.FileField(upload_to='banderas', null=True, blank=True)
    def __str__(self):
        return self.nombre

class Localidades(models.Model):
    nombre= models.CharField(max_length=30, unique=True)
    def __str__(self):
        return self.nombre

class Estadios(models.Model):
    id_localidad= models.ForeignKey(Localidades, on_delete=models.CASCADE)
    nombre= models.CharField(max_length=30)
    def __str__(self):
        return self.nombre

class Eventos(models.Model):
    fecha_hora= models.DateTimeField()
    id_deporte= models.ForeignKey(Deporte, on_delete=models.CASCADE)
    id_estadio= models.ForeignKey(Estadios, on_delete=models.CASCADE)
	
    def __str__(self):
        return self.id_deporte.nombre+' '+self.id_estadio.nombre+' '+str(self.fecha_hora)

class Transmisiones(models.Model):
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    id_evento= models.ForeignKey(Eventos, on_delete=models.CASCADE)
    canal_tv= models.CharField(max_length=30)
    link= models.URLField()
    fecha_transmision= models.DateField()
    hora_transmision= models.TimeField()
    estado_eliminado= models.BooleanField(default=False)
    def __str__(self):
        return self.canal_tv +' '+self.link

class Resenias(models.Model):
    id_evento= models.ForeignKey(Eventos, on_delete=models.CASCADE, null=True)
    id_usuario_autor= models.ForeignKey(User, on_delete=models.CASCADE)
    titulo= models.CharField(max_length=50)
    contenido= models.TextField()
    estado_eliminado= models.BooleanField(default=False)
    valoracion = models.FloatField(default=0)
    def __str__(self):
        return self.titulo

class Paises_Deportes(models.Model):
    id_pais= models.ForeignKey(Paises, on_delete=models.CASCADE,)
    id_deporte= models.ForeignKey(Deporte, on_delete=models.CASCADE,)
	
    class Meta:
        unique_together= (('id_pais','id_deporte'),)

class Eventos_Paises(models.Model):
    id_pais= models.ForeignKey(Paises, on_delete=models.CASCADE)
    id_evento= models.ForeignKey(Eventos, on_delete=models.CASCADE)

class Resenias_Valoraciones(models.Model):
    id_resenia= models.ForeignKey(Resenias, on_delete=models.CASCADE)
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    valor=models.PositiveIntegerField()
	
    class Meta:
        unique_together= (('id_resenia','id_usuario'),)

class Imagenes(models.Model):
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    id_evento= models.ForeignKey(Eventos, on_delete=models.CASCADE, null=True)
    imagen= models.FileField(upload_to='images', null=True, blank=True)
    descripcion= models.CharField(max_length=140)
    estado_eliminado= models.BooleanField(default=False)

class Comentarios(models.Model):
    id_usuario_autor= models.ForeignKey(User, on_delete=models.CASCADE)
    id_evento= models.ForeignKey(Eventos, on_delete=models.CASCADE, null=True)
    descripcion= models.CharField(max_length=140)
    estado_eliminado= models.BooleanField(default=False)
    def __str__(self):
	    return self.descripcion

class Comentarios_Resenias(models.Model):
    id_usuario_autor= models.ForeignKey(User, on_delete=models.CASCADE)
    id_resenia= models.ForeignKey(Resenias, on_delete=models.CASCADE, null=True)
    descripcion= models.CharField(max_length=140)
    estado_eliminado= models.BooleanField(default=False)
    def __str__(self):
	    return self.descripcion
		
class Encuestas(models.Model):
    id_usuario_creador= models.ForeignKey(User, on_delete=models.CASCADE)
    titulo= models.CharField(max_length=50)
    descripcion= models.TextField()
    id_evento= models.ForeignKey(Eventos, on_delete=models.CASCADE, null=True)
    fecha_cierre= models.DateTimeField()
    estado_eliminado= models.BooleanField(default=False)
    def __str__(self):
        return self.titulo

class Valores_Encuestas(models.Model):
    id_encuesta= models.ForeignKey(Encuestas, on_delete=models.CASCADE,)
    valor= models.CharField(primary_key=True, max_length=30)
	
    class Meta:
        unique_together= (('id_encuesta','valor'),)
	
    def __str__(self):
        return self.valor

class Resultados_Encuestas(models.Model):
    id_encuesta= models.ForeignKey(Encuestas, on_delete=models.CASCADE,)
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE,)
    valor_encuesta=models.ForeignKey(Valores_Encuestas, on_delete=models.CASCADE)
	
    class Meta:
        unique_together= (('id_encuesta','id_usuario'),)

class Motivos(models.Model):
    descripcion= models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion

class Usuarios_Historial(models.Model):
    fecha_hora= models.DateTimeField()
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    #tipo_bloqueo= models.BooleanField()
	
class Pregunta_Foro(models.Model):
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    pregunta= models.CharField(max_length=100)
    estado_eliminado= models.BooleanField(default=False)
	
    def __str__(self):
        return self.pregunta
 
class Respuesta_Foro(models.Model):
    id_usuario= models.ForeignKey(User, on_delete=models.CASCADE)
    id_pregunta_foro= models.ForeignKey(Pregunta_Foro, on_delete=models.CASCADE)
    respuesta= models.TextField(max_length=200)
    estado_eliminado= models.BooleanField(default=False)
	
    def __str__(self):
        return self.respuesta

	
class Denuncias_Comentarios(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_comentario= models.ForeignKey(Comentarios, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_comentario'),)
		
class Denuncias_Comentarios_Resenias(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_comentario= models.ForeignKey(Comentarios_Resenias, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_comentario'),)

class Denuncias_Imagenes(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_imagen= models.ForeignKey(Imagenes, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_imagen'),)

class Denuncias_Resenias(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_Resenias= models.ForeignKey(Resenias, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_Resenias'),)

class Denuncias_Transmisiones(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_transmision= models.ForeignKey(Transmisiones, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_transmision'),)
		
class Denuncias_Encuestas(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_encuesta= models.ForeignKey(Encuestas, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_encuesta'),)
		
class Denuncias_Preguntas(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_pregunta= models.ForeignKey(Pregunta_Foro, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_pregunta'),)

class Denuncias_Respuestas(models.Model):
    id_denunciador= models.ForeignKey(User, on_delete=models.CASCADE,)
    id_respuesta= models.ForeignKey(Respuesta_Foro, on_delete=models.CASCADE,)
    id_motivo= models.ForeignKey(Motivos, on_delete=models.CASCADE)
    fecha_hora_denuncia= models.DateTimeField()
	
    class Meta:
        unique_together= (('id_denunciador','id_respuesta'),)
    



